package teamproject;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ArithmeticTest {
	
	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testInt_operation() {
		Arithmetic.int_operation();
	}

	@Test
	public void testFra_operation() {
		Arithmetic.fra_operation();
	}

	@Test
	public void testcommon_divisor() {
		Arithmetic.common_divisor(2, 2);
		Arithmetic.common_divisor(3, 99);
	}
}
